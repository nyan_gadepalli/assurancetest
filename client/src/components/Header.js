import React, { Component } from 'react'
import PropTypes from 'prop-types'
import TodoTextInput from './TodoTextInput'
import {connect} from 'react-redux'
import {
  buttonStyle, buttonDivStyle, divStyle,
  hStyle, wordListStyle, totalStyle
} from '../styles/styles'
import {Board} from './Board'

class Header extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired
  }

  handleSave = text => {
    if (text.length !== 0) {
      const {gameID, words} = this.props
      this.props.actions.checkWords(gameID, text, words)
    }
  }

  render() {
    return (
      <header className="header">

        <div style={buttonDivStyle}>
          <h1 style={hStyle}>Boggle</h1>
          <button style={buttonStyle} onClick={this.props.actions.newGame}> New Game </button>
          <button style={buttonStyle} onClick={this.props.actions.done}> Done </button>
        </div>

        <div style={divStyle}><Board words={this.props.board}/></div>

        <div style={divStyle}>

          <div style = {totalStyle}>
            {total(this.props)}
          </div>

          <div style={wordListStyle}>
            {wordList(this.props)}
          </div>

        </div>
        <TodoTextInput
          newTodo
          onSave={this.handleSave}
         placeholder="Type your words here..." />
      </header>
    )
  }
}

/* Word list component (eventually)*/
const wordList = (props) => {
  const {words, scores} = props
  const isValid = (index) => (scores[index]===0)?" - INVALID! - ": " - VALID - "
  const listItems = words.map((word, index) =>
    <li key={index}>{word}{isValid(index)} with {scores[index]}  </li>
  );
  return <ul>{listItems}</ul>
}

/*Dashboard component (eventually*/
const total = (props) => {
  const {total, done} = props
  if (total===undefined) {
    const message =
      ("Press 'New Game' to start a new game")
    return message
  }
  else if (done) {
    const congrats =
      ("Congratulations!!, you scored " + total + " points!!! ")
    const playAgain =
      ("Press 'New Game' to play again ")
    const message = congrats + playAgain
    return message
  }
  else {
    const message = ("Total Score: " + total)
    return message
  }
}

const mapStateToProps = (state) => {
  return {
    board: state.game.board,
    words: state.game.words,
    scores: state.game.scores,
    gameID: state.game.gameID,
    total: state.game.total,
    done: state.game.done
  }
}
export default connect(mapStateToProps)(Header)
