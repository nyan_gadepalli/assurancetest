import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Square} from './Square'
import {boardRowStyle} from '../styles/styles'


export class Board extends Component {
  static propTypes = {
    words: PropTypes.array.isRequired
  }

  renderSquare(value) {
    return (
      <Square value={value}/>
    );
  }

  render() {
    return (
      <div>
      <div style={boardRowStyle}>
      {this.renderSquare(this.props.words[0])}\
      {this.renderSquare(this.props.words[1])}
      {this.renderSquare(this.props.words[2])}
      {this.renderSquare(this.props.words[3])}
      </div>

      <div style={boardRowStyle}>
      {this.renderSquare(this.props.words[4])}
      {this.renderSquare(this.props.words[5])}
      {this.renderSquare(this.props.words[6])}
      {this.renderSquare(this.props.words[7])}
      </div>

      <div style={boardRowStyle}>
      {this.renderSquare(this.props.words[8])}
      {this.renderSquare(this.props.words[9])}
      {this.renderSquare(this.props.words[10])}
      {this.renderSquare(this.props.words[11])}
      </div>

      <div style={boardRowStyle}>
      {this.renderSquare(this.props.words[12])}
      {this.renderSquare(this.props.words[13])}
      {this.renderSquare(this.props.words[14])}
      {this.renderSquare(this.props.words[15])}
      </div>
      </div>
    );
  }

}
