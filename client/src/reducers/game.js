import {
  NEW_GAME_API_200, NEW_GAME_API_ERROR,
  CHECK_WORD_API_200, CHECK_WORD_API_ERROR, DONE
} from '../constants/ActionTypes'

const initialState = {
  board: [],
  words: [],
  done:false
}

export default function game(state = initialState, action) {
  switch (action.type) {
    case NEW_GAME_API_200:
      const jBoard = JSON.parse(action.game.board)
      const gameID = action.game.id
      return {
        board: jBoard,
        gameID: gameID,
        words: [],
        total: 0
      }
    case NEW_GAME_API_ERROR:
      console.log(action.error)
      return action.error
    case CHECK_WORD_API_200:
      const jWords = JSON.parse(action.game.words)
      const jScores = JSON.parse(action.game.scores)
      return {
        ...state, words:jWords, scores: jScores, total: jScores.reduce((a, b) => a + b, 0)
      }
    case CHECK_WORD_API_ERROR:
      console.log(action.error)
      return action.error
    case DONE:
      return {
        ...state, done:true
      }
    default:
      return state
  }
}
