class AddScoresToGame < ActiveRecord::Migration[5.1]
  def change
    add_column :games, :scores, :string
  end
end
