export const buttonStyle = {
  background:'lightgrey', height:'60px', width:'200px', color:'white',
  fontWeight:'bold', cursor:'pointer', marginLeft:'45px', padding:'10px',
  textAlign:'center', position:'relative', marginTop:'100px'
}

export const buttonDivStyle = {
  background: '#f5f5f5',
  minHeight:'300px'
}

export const wordListStyle = {
  background:'#f5f5f5',
  minWidth:'300px',
  float:'left'
}

export const totalStyle = {
  background:'#f5f5f5',
  minWidth:'300px',padding:'15px',
}
export const divStyle = {
  background:'#f5f5f5',
  minHeight:'200px'
}

export const hStyle = {
  position:'relative',
  top:'80px', color:'lightgrey'
}

export const squareStyle = {
  background: '#fff',
  border: '1px solid #999',
  float: 'left',
  fontSize: '24px',
  fontWeight: 'bold',
  lineHeight: '34px',
  height: '40px',
  marginRight: '-1px',
  marginTop: '-1px',
  padding: 0,
  textAlign: 'center',
  width: '40px',
  marginLeft:'80px'
}

export const boardRowStyle = {
  minHeight:'60px'
}
