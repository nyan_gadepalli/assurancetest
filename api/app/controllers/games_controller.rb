class GamesController < ApplicationController
  before_action :set_game, only: [:show, :update, :destroy, :check_word]
  # GET /games
  def index
    @games = Game.all

    render json: @games
  end

  # GET /games/1
  def show
    render json: @game
  end

  # POST /games
  def create
    @game = Game.new(game_params)

    if @game.save
      render json: @game, status: :created, location: @game
    else
      render json: @game.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /games/1
  def update
    if @game.update(game_params)
      render json: @game
    else
      render json: @game.errors, status: :unprocessable_entity
    end
  end

  def check_word
    #Retrieve and Package Data
    words = JSON.parse(game_params[:words])
    board = JSON.parse(@game.board)
    adjacency = JSON.parse(@game.adjacency)
    word = game_params[:word]
    words.push(word)
    scores = if @game.scores then JSON.parse(@game.scores) else [] end

    #Run Path Algorithm - Find Possible Paths through Board for Word
    paths = build_paths(word, board, adjacency)
    puts paths

    #Ask Dictionary
    isRealWord = false
    begin
      response = RestClient::Request.execute(
        method: :get, url: ('https://wordsapiv1.p.mashape.com/words/' + word),
        headers:{
          "X-Mashape-Key" => "kFb9tWvNflmshejTZhkQgGJEJ9MCp1Nz4LKjsnz3RJredEMZTf",
          "Accept" => "application/json"
      })
      isRealWord = JSON.parse(response).key?("results")
    rescue
      isRealWord = false
    end

    #Scoring
    if paths.size > 0 and isRealWord then
      scores.push(word.length*10)
    else scores.push(0) end

    #Respond
    if @game.update({words: words.to_json, scores: scores.to_json})
      render json: @game
    else
      render json: @game.errors, status: :unprocessable_entity
    end
  end

  # DELETE /games/1
  def destroy
    @game.destroy
  end

  def build_paths(word, board, adjacency)
    paths = []

    #Scan characters of word
    word.chars.to_a.each_with_index do |letter, position|

      #First character of word
      if position == 0
        #Scan through board, create possible paths based on possible starting points
        board.each_with_index do |char, index|
            if letter == char
              point = {index:index, position: position, letter: letter}
              paths.push({points:[point], indices:[point[:index]]})
            end
          end
      else
        #Not first character of word
        #Scan through board, append to paths if point is adjacent to a path
        #If a path diverges in the middle and a prior point has been appended, duplicate
        #the path, remove the prior point, append the point, update the paths array.
        #all paths should be size 'position'
        board.each_with_index do |char, index|
          if letter == char
            point = {index: index, position: position, letter: letter}
            paths = appendToOrAddPaths(adjacency, paths, point)
          end
        end
        paths = prunePaths(paths, position)
      end
    end
    return paths
  end


  def prunePaths(paths, position)
    paths.each_with_index do |path, number|
      if (path[:points].size < position)
        paths.delete(path)
      end
    end
      return paths
  end

  def appendToOrAddPaths(adjacency, paths, point)
    paths.each_with_index do |path, number|
      path = appendToPath(adjacency, path, point)
      paths = addExtraPath(adjacency, path, paths, point)
    end
  end

  def appendToPath(adjacency, path, point)
    if (path[:points].size == point[:position])
      last_point = path[:points][-1]
      adjacent = adjacentCheck(adjacency, point[:index], last_point, path[:indices])
      if adjacent
        path[:points].push(point)
        path[:indices].push(point[:index])
      end
    end
    return path
  end

  def addExtraPath(adjacency, path, paths, point)
    if (path[:points].size > point[:position])
      last_point = path[:points][point[:position]-2]
      adjacent = adjacentCheck(adjacency, point[:index], last_point, path[:indices])
      if adjacent
        extra_path = path
        extra_path[:points].pop()
        extra_path[:indices].pop()
        extra_path[:points].push(point)
        extra_path[:indices].push(point[:index])
        paths.push(extra_path)
        return paths
      end
    end
    return paths
  end

  def adjacentCheck(adjacency, index, last_point, indices)
    return (adjacency[index].include?last_point[:index] and
            not indices.include?index)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game
      @game = Game.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def game_params
      params.require(:game).permit(:adjacency, :board, :word, :words)
    end
end
