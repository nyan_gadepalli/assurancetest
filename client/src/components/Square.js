import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {squareStyle} from '../styles/styles'


export class Square extends Component {
  static propTypes = {
    value: PropTypes.string,
  }
  render() {
    return (
      <button style={squareStyle}>
        {this.props.value}
        </button>
    );
  }
}
