class CreateGames < ActiveRecord::Migration[5.1]
  def change
    create_table :games do |t|
      t.string :adjacency
      t.string :board
      t.string :words

      t.timestamps
    end
  end
end
