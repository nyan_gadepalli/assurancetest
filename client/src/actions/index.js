import * as types from '../constants/ActionTypes'
import * as routes from '../constants/Routes'
import * as game from '../constants/Game'
import fetch from 'isomorphic-fetch'

/*Game Actions*/
export const newGameApiCall =
  () => ({type: types.NEW_GAME_API_CALL})

/*Thunk - action executed by middleware - handles newGame server interaction*/
export function newGame() {
  return function (dispatch) {
    dispatch(newGameApiCall())

    const board = JSON.stringify(game.randomBoard())
    const adjacency = JSON.stringify(game.ADJACENCY)
    const headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
    const data = JSON.stringify({game: {board: board,adjacency: adjacency}})

    return fetch(
      routes.NEW_GAME, {method: 'POST',headers: headers, body: data
    })

    .then(r => r.json()).then(r => dispatch(newGameApi200(r)))
    .catch(e => dispatch(newGameApiError(e)))
  }
}

export const newGameApi200 =
  (g) => ({type: types.NEW_GAME_API_200, game: g})

export const newGameApiError =
  (e) => ({type: types.NEW_GAME_API_ERROR, error: e})

export const checkWordsApiCall =
  () => ({type: types.CHECK_WORD_API_CALL})

/*Thunk - action executed by middleware - handles checkWord server interaction*/
export function checkWords(gameID, word, wordList) {
    return function (dispatch) {
      dispatch(checkWordsApiCall())

      const jWords = JSON.stringify(wordList)
      const headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
      const data = JSON.stringify({id: gameID, game:{word:word, words: jWords}})

      return fetch(
        routes.CHECK_WORD, {method: 'POST',headers: headers,body: data,}
      )
      .then(r => r.json())
      .then(r => dispatch(checkWordsApi200(r)))
      .catch(e => dispatch(checkWordsApiError(e)))
    }
  }

export const checkWordsApi200 =
  (g) => ({type: types.CHECK_WORD_API_200, game:g})

export const checkWordsApiError =
  (e) => ({type: types.CHECK_WORD_API_ERROR, error: e})

export const done =
  () => ({type: types.DONE})
